<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/NestZCSS.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Mitr|Prompt" rel="stylesheet">
    <title>Report page by NestZ</title>
    <script>
        function getSelectedValue(){
            var subDisSelect = document.querySelector("#s1").value;
            var disSelect = document.querySelector("#s0").value;
            document.getElementById("subDis").innerHTML = subDisSelect;
            document.getElementById("dis").innerHTML = disSelect;
        }
    </script>
</head>
<body onload="getSelectedValue();">

        

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 main">
            <div class="row">
                <img src="{{ asset('images/logo/report_logo.png') }}" width="100%">
            </div>
            <div class="row" width="auto">
                <p align="left" id="header">เลือกพื้นที่</p>
            </div>
            <div class="row outer-flex-container" width="100%">
                    <div>
                        <p>จังหวัด : </p>
                        <select>
                            <option value="0">เชียงราย</option>
                        </select>
                    </div>
                    <div>
                        <p>อำเภอ : </p>
                        <select id="s0" onchange="getSelectedValue();">
                            <option value="-เลือกอำเภอ-">-- เลือกอำเภอ --</option>
                            <option value="อำเภออะไรก็ได้1">อำเภออะไรก็ได้1</option>
                            <option value="อำเภออะไรก็ได้2">อำเภออะไรก็ได้2</option>
                        </select>
                    </div>
                    <div>
                        <p>ตำบล : </p>
                        <select id="s1" onchange="getSelectedValue();">
                            <option value="-เลือกตำบล-">-- เลือกตำบล --</option>
                            <option value="ตำบลไรดี">ตำบลไรดี</option>
                            <option value="นั่นสินะ">นั่นสินะ</option>
                        </select> 
                    </div>
            </div>
            <div class="row" style="margin: 50px;">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div id="map" style="width: 100%; height: 600px" align="center"></div>
                </div>
            </div>
            <script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script>
            <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
            <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
            <!-- Include this library for mobile touch support  -->
            <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
            <script src="{{ asset('js/SliderControl.js') }}"></script>
            <script>
            var sliderControl = null;
            var myMap = L.map('map').setView([52.06, 7.40], 10);

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(myMap);

            //Use usual LayerGroups instead of GeoJSON fetched markers
            /*
            var marker1 = L.marker([51.933292258, 7.582512743], {time: "2013-01-22 08:42:26+01"});
            var marker2 = L.marker([51.9, 7.6], {time: "2013-01-22 10:00:26+01"});
            var marker3 = L.marker([51.7, 7.7], {time: "2013-01-22 10:03:29+01"});
                
            var pointA = new L.LatLng(51.8, -0.09);
            var pointB = new L.LatLng(51.9, -0.2);
            var pointList = [pointA, pointB];

            var polyline = new L.Polyline(pointList, {
            time: "2013-01-22 10:24:59+01",
            color: 'red',
            weight: 3,
            opacity: 1,
            smoothFactor: 1
            });
                
            layerGroup = L.layerGroup([marker1, marker2, marker3, polyline]);
            var sliderControl = L.control.sliderControl({layer:layerGroup, follow: true});
            myMap.addControl(sliderControl);
            sliderControl.startSlider();
            */

            //Fetch some data from a GeoJSON file
            $.getJSON("{{ asset('json/points.json') }}", function(json) {
                var testlayer = L.geoJson(json),
                    sliderControl = L.control.sliderControl({
                        position: "bottomleft",
                        layer: testlayer
                    });

                //For a Range-Slider use the range property:
                sliderControl = L.control.sliderControl({
                    position: "bottomleft",
                    layer: testlayer,
                    range: true
                });
                   
                //Make sure to add the slider to the map ;-)
                myMap.addControl(sliderControl);
                //And initialize the slider
                sliderControl.startSlider();
            });
            </script>
            <div class="row bottom-flex-container" width="100%">
                <div>
                    <p sytyle="display: inline;">   ข้อมูลตำบล  :</p>
                    <span id="subDis"></span>
                </div>
                <div>
                    <p sytyle="display: inline;">   อำเภอ  :</p>
                    <span id="dis"></span>
                </div>
            </div>
            <table width="100%" class="bottomTable">
                <tr>
                    <td>NO.</td>
                    <td>รหัสสิ่งกีดขวาง</td>
                    <td>หมู่บ้าน</td>
                    <td>ที่ตั้ง</td>
                    <td>วันที่</td>
                </tr>
                <?php
                for($i = 0;$i < count($data);$i++){
                ?>
                <tr>
                    <td>{{$i+1}}</td>
                    <td>{{$data[$i]->blockage->blk_id}}</td>
                    <td>{{$data[$i]->blk_village}}</td>
                    <td>ต.{{$data[$i]->blk_tumbol}} อ.{{$data[$i]->blk_district}}</td>
                    <td>{{date('d/m/Y',strToTime($data[$i]->blockage->created_at))}}</td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <div class="col-sm-2"></div>
    </div>
</body>
</html>