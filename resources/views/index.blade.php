<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jiwcss.css') }}">
        <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" type="text/css">
    </head>
    <body>
        <div class="row">
            <div class="col-sm-1"> </div>
            <div class="col-sm-10">
                <img src="{{ asset('images/logo/Logojiw.png') }}" width="100%">
            </div>
            <div class="col-sm-1"> </div>
        </div>
        <div class="row jiw">
            <div class="col-sm-1"> </div>
            <div class="col-sm-2">
                <a href="#" class="button1">กรอกแบบฟอร์ม</a>
                <a href="#" class="button2">รายงานสิ่งกีดขวาง</a>
            </div>
            <div class="col-sm-8"> <!--\\\\\\\\\\\\\\\\\\\\\\\\\\\Map\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-->
                <div class="row" style="margin: 50px;">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div id="map" style="width: 100%; height: 600px" align="center"></div>
                        </div>
                    </div>
                <script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script>
                <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
                <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
                <!-- Include this library for mobile touch support  -->
                <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
                <script src="{{ asset('js/SliderControl.js') }}"></script>
                <script>
                    var sliderControl = null;
                    var myMap = L.map('map').setView([52.06, 7.40], 10);
                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {}).addTo(myMap);
                    //Fetch some data from a GeoJSON file
                    $.getJSON("{{ asset('json/points.json') }}", function(json) {
                        var testlayer = L.geoJson(json),
                        sliderControl = L.control.sliderControl({
                            position: "bottomleft",
                            layer: testlayer
                            });
                        //For a Range-Slider use the range property:
                        sliderControl = L.control.sliderControl({
                            position: "bottomleft",
                            layer: testlayer,
                            range: true
                            });
                        //Make sure to add the slider to the map ;-)
                        myMap.addControl(sliderControl);
                        //And initialize the slider
                        sliderControl.startSlider();
                    });
                </script>
            </div> <!--\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-->
            <div class="col-sm-1"> </div>
            </div>
        </div>
    </body>
</html>