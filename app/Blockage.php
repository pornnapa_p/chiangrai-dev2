<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Blockage extends Model
{
    //
    protected $fillable = [
        'blk_id',
        'blk_code',
        'blk_location_id',
        'river_id',
        'blk_crossection_id',
        'sol_id',
        'blk_length',
        'damage_type',
        'damage_level',
        'damage_frequency',
        'blk_surface',
        'blk_surface_detail'
    ];

    public function blockageLocation() {
        return $this->hasOne('App\BlockageLocation', 'blk_location_id', 'blk_location_id');
    }

    public function blockageCrossection() {
        return $this->hasOne('App\BlockageCrossection', 'blk_xsection_id', 'blk_crossection_id');
    }
    public function River() {
        return $this->hasOne('App\River', 'river_id', 'river_id');
    }
    public function Solution() {
        return $this->hasOne('App\Solution', 'sol_id', 'sol_id');
    }
    public function Photo() {
        return $this->hasMany('App\Photo', 'blk_id', 'blk_id');
    }

    public static function getBlockage($blk_id=0){

        $value=DB::table('blockages')->where('blk_id', $blk_id)->distinct()->get();

        return $value;
    }


}
