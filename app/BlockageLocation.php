<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class BlockageLocation extends Model
{
    use SpatialTrait;
    // protected $primaryKey = 'blk_location_id';

    protected $fillable = [
        'blk_location_id',
        'blk_start_location',
        'blk_end_location',
        'blk_village',
        'blk_tumbol',
        'blk_district',
        'blk_province',
        'created_at',
        'updated_at'

      ];
      protected $spatialFields = [
        'blk_start_location',
        'blk_end_location' 
    ];

    public function blockage() {
      return $this->hasOne('App\Blockage', 'blk_location_id', 'blk_location_id');
    }
    
}
