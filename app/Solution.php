<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    protected $fillable = [
       'sol_id',
       'proj_id',
       'responsed_dept',
       'sol_how',
       'result',
       'created_at',
       'created_at'
    ];
}
