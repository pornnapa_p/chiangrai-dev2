<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlockageLocation;
use App\Blockage;
use App\BlockageCrossection;
use App\River;
use App\ProblemDetail;
use App\Solution;
use App\Project;
use DB;
use Auth;

use Grimzy\LaravelMysqlSpatial\Types\Point;

class FormBlockageController extends Controller
{
    public function formblockage()
    {
        return view('form_blockage');
    }
    public function storeform(Request $request){
      //dd($request);
    //  $locationloc= DB::select('SELECT *FROM `blockage_locations`  order by `created_at` DESC LIMIT 1');
    //  dd($locationloc->blk_location_id);
    
    function calCode($users,$text) {
       
        if($users== NULL){
            return ("00001");
        }else{
            
            $names = str_split($users->$text);
            if($text=="prob_id" ||$text=="proj_id" ){
                $code =$names[3].$names[4].$names[5].$names[6];
            }else{
                $code =$names[2].$names[3].$names[4].$names[5];
            }
            $num=$code+1;

            if($num<10){
                return ("0000".$num);
            }else if ($num<100){
                return ("000".$num);
            }else if ($num<1000){
                return ("00".$num);
            }else {
                return ("0".$num);
            }
        }
        
    }
    
    $locations = DB::table('blockage_locations')->select('blk_location_id')->orderBy('created_at', 'asc')->get()->last();
    $crossection = DB::table('blockage_crossections')->select('blk_xsection_id')->orderBy('created_at', 'asc')->get()->last();
    $river = DB::table('rivers')->select('river_id')->orderBy('created_at', 'asc')->get()->last();
    $problem = DB::table('problem_details')->select('prob_id')->orderBy('created_at', 'asc')->get()->last();
    $solution = DB::table('solutions')->select('sol_id')->orderBy('created_at', 'asc')->get()->last();
    $project = DB::table('projects')->select('proj_id')->orderBy('created_at', 'asc')->get()->last();
    $blockage = DB::table('blockages')->select('blk_id')->orderBy('created_at', 'asc')->get()->last();
   
   // echo ($problem);
    $locationsId="L".calCode($locations,"blk_location_id");
    $crossectionId="X".calCode($crossection,"blk_xsection_id");
    $riverId="R".calCode($river,"river_id");
    $problemId="PB".calCode($problem,"prob_id");
    $solutionId="S".calCode($solution,"sol_id");
    $projectId="PJ".calCode($project,"proj_id");
    $blockageId="B".calCode($blockage,"blk_id");
    //echo ($projectId."/".$blockageId);
    $vill=explode(" ",$request->blk_village);
    $code =DB::table('info_village')->select('vill_code')->where('vill_name',$vill[2] )->where('vill_moo',$vill[1])->get();
    $codeBlk=$code[0]->vill_code;
    
    //location point
        $locationSt = new Point($request->latstart,$request->longstart);
        $locationSt = new Point($request->latstart,$request->longstart);
        $locationFin = new Point($request->latend,$request->longend);

        /////////--------blockage_location-------------/////////
        $loc = new BlockageLocation(
            [
                'blk_location_id'=>$locationsId ,
                'blk_start_location'=>$locationSt,
                'blk_end_location'=>$locationFin,
                'blk_village'=>$request->blk_village,
                'blk_tumbol'=>$request->blk_tumbol,
                'blk_district'=>$request->blk_district,
                'blk_province'=>$request->blk_province
            ]
        );
         $loc->save();
         /////////--------blockage_crossection-------------/////////
      
        $BlockageCrossection = new BlockageCrossection(
            [
                'blk_xsection_id'=>$crossectionId,
                'blk_id'=>$blockageId,
                'past'=>json_encode($request->past),
                'current_start'=>json_encode($request->current_start),
                'current_narrow'=>json_encode($request->current_narrow),
                'current_end'=>json_encode($request->current_end),
            ]
        );
        $BlockageCrossection->save();
        /////////--------River-------------/////////
        $River = new River(
            [
               'river_id'=>$riverId,
               'river_name'=>$request->river_name,
               'river_type'=>$request->river_type,
               'river_main'=>$request->river_main
            ]
        );
        $River->save();
        
        

         /////////--------addSolution-------------/////////
        $solutionLoc = new Solution(
            
            [
                'sol_id'=>$solutionId,
                'proj_id'=>$projectId,
                'responsed_dept'=>$request->responsed_dept,
                'sol_how'=> $request->sol_how,
                'result'=>$request->result_selector
            ]
        );
         $solutionLoc->save();

           ///////--------blockage Main-------------/////////
         
         $Blockage = new Blockage(
             [
                 'blk_id' =>$blockageId,
                 'blk_code'=>$codeBlk,
                 'blk_location_id' => $locationsId,
                 'river_id' => $riverId,
                 'blk_crossection_id' =>$crossectionId,
                 'sol_id' =>$solutionId,
                 'blk_length' =>$request->blk_length,
                 'damage_type' => $request->damage_type,
                 'damage_level' => json_encode($request->damage_level),
                 'damage_frequency' => $request->damage_frequency,
                 'blk_surface'=>$request->blk_surface,
                 'blk_surface_detail'=>$request->blk_surface_detail

             ]
         );
         $Blockage->save();
        // $Blockage = new Blockage(
        //     [
        //         'blk_id' =>$blockageId,
        //         'blk_code'=>"CR100301",
        //         'blk_location_id' => "L00001",
        //         'river_id' => "R00001",
        //         'blk_crossection_id' =>"X00001",
        //         'sol_id' =>"S00001",
        //         'blk_length' =>"10",
        //         'damage_type' => "น้ำ",
        //         'damage_level' => "น้ำน้ำ",
        //         'damage_frequency' =>"น้ำน้ำ",
        //         'blk_surface'=>"น้ำน้ำ",
        //         'blk_surface_detail'=>"น้ำน้ำ"

        //     ]
        // );
        // $Blockage->save();
          

        /////////--------ProblemDetail-------------/////////
        $ProblemCount = ProblemDetail::count();
        $Promblemloc = new ProblemDetail(
            
            [
                'prob_id'=> $problemId,
                'blk_id'=>$blockageId,
                'prob_level'=>$request->prob_level,
                'nat_erosion'=>$request->nat_erosion,
                'nat_shoal'=>$request->nat_shoal,
                'nat_missing'=>$request->nat_missing,
                'nat_winding'=>$request->nat_winding,
                'nat_weed'=>$request->nat_weed,
                'nat_weed_detail'=>$request->nat_weed_detail,
                'nat_other'=>$request->nat_other,
                // 'nat_other_detail'=>$request->nat_other_detail,
                'nat_other_detail'=>"1",
                'hum_structure'=>$request->hum_structure,
                'hum_str_owner_type'=>$request->hum_str_owner_type,
                'hum_stc_bld_num'=>$request->hum_stc_bld_num,
                'hum_stc_fence_num'=>$request->hum_stc_fence_num,
                'hum_str_other'=>$request->hum_str_other,
                'hum_road'=>$request->hum_road,
                'hum_smallconvert'=>$request->hum_smallconvert,
                'hum_road_paralel'=>$request->hum_road_paralel,
                'hum_replaced_convert'=>$request->hum_replaced_convert,
                'hum_bridge_pile'=>$request->hum_bridge_pile,
                'hum_soil_cover'=>$request->hum_soil_cover,
                'hum_trash'=>$request->hum_trash,
                'hum_other'=>$request->hum_other,
                'hum_other_detail'=>$request->hum_other_detail,

            ]
        );
         $Promblemloc->save();


         
       return redirect()->route("form.Qnaire4");
    }


    public function getBlockageData() {
        $data = Blockage::with('blockageLocation','blockageCrossection','River','Solution','Photo')->get();
        return response()->json($data);
    }

    public function getBlockageMap() {
        $data = DB::table('blockage_locations')->select('updated_at','blk_start_location')->orderBy('created_at', 'asc')->get();
        $result=[];
        $properties['time']=[];
        
        for ($i=0;$i<count($data);$i++){
            //$locationSt = new Point($request->latstart,$request->longstart);
            $point =($data[0]->blk_start_location);
            $result[] = [
                'type' => "Feature",
                'properties' => [
                    'time'=> $data[$i]->updated_at],
                'geometry'=>$point
            ];           
         }
        $test['type']="FeatureCollection";
        $test['features']=$result;
    
         $test = json_encode($test);
         echo $test;
        //return response()->json($data);
        
    }



    public function getBlockageID($blk_id=0){
        $data = Blockage::with('blockageLocation','blockageCrossection','River','Solution','Photo')->where('blk_id', $blk_id)->distinct()->get();
        return response()->json($data);
        exit;
    }

    public function getBlockageTumbon($tumBon=0){
        $data = BlockageLocation::with('blockage')->where('blk_tumbol', $tumBon)->get();
        return view('report',compact('data'));
    }
}
