<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PagesController extends Controller
{
    public function indexdistrict(){

        // Fetch departments
        $districtData['data'] = Page::getDistrict();
       // dd($districtData['data']);
        // Load index view
        return view('indexdistrict')->with("districtData",$districtData);
        
      }

      public function formblockage(){

        // Fetch departments
        $districtData['data'] = Page::getDistrict();
       // dd($districtData['data']);
        // Load index view
        return view('form_blockage')->with("districtData",$districtData);
        
      }

     // Fetch tumbol
    public function getTumbol($vill_districtid=0){

        // Fetch Employees by Departmentid
        $userData['data'] = Page::getdistrictTumbol($vill_districtid);

        echo json_encode($userData);
        exit;
    }

     // Fetch rvillage
     public function getVillage($vill_tumbolid=0){

        // Fetch Employees by Departmentid
        $userVill['data'] = Page::gettumbolVillage($vill_tumbolid);

        echo json_encode($userVill);
        exit;
    }
}
