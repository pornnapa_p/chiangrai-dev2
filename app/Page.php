<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model
{
// Fetch district
   public static function getDistrict(){
    $value=DB::table('info_village')->distinct()->get('vill_district'); 
    //$value=DB::table('villages')->distinct()->get(); 
    return $value;
  }

    // Fetch Tumtol
    public static function getdistrictTumbol($vill_districtid=0){

        
        $value=DB::table('info_village')->where('vill_district', $vill_districtid)->distinct()->get('vill_tunbol');

        return $value;
    }

    //Fetch Tumtol
    public static function gettumbolVillage($vill_tumbolid=0){

        $value=DB::table('info_village')->where('vill_tunbol', $vill_tumbolid)->distinct()->get();

        return $value;
    }


}
