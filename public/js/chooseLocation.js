    $(document).ready(function(){

      // District Change
      $('#blk_district').change(function(){

         // District name
         var id = $(this).val();
         
         // Empty the dropdown
         $('#blk_tumbol').find('option').not(':first').remove();

         // AJAX request 
         $.ajax({
           url: 'getTumbol/'+id,
           type: 'get',
           dataType: 'json',
           success: function(response){

             var len = 0;
             if(response['data'] != null){
               len = response['data'].length;
             }

             if(len > 0){
               // Read data and create <option >
               for(var i=0; i<len; i++){

                 var id = response['data'][i].vill_id;
                 var name = response['data'][i].vill_tunbol;

                 var option = "<option value='"+name+"'>"+name+"</option>"; 

                 $("#blk_tumbol").append(option); 
               }
             }

           }
        });
      });

    });


    $(document).ready(function(){

      // Tombol Change
      $('#blk_tumbol').change(function(){

         // Tombol name
         var id = $(this).val();
         //var id2='แม่จัน';
         //alert(id);
         //alert(id2);

         // Empty the dropdown
         $('#blk_village').find('option').not(':first').remove();

         // AJAX request 
         $.ajax({
           url: 'getVillage/'+id,
           type: 'get',
           dataType: 'json',
           success: function(response){

             var len = 0;
             if(response['data'] != null){
               len = response['data'].length;
             }

             if(len > 0){
               // Read data and create <option >
               for(var i=0; i<len; i++){


                 var name = response['data'][i].vill_name;
                 var moo = response['data'][i].vill_moo;
                 var village ="หมู่ที่ "+moo+" "+name;

                 var option = "<option value='"+village+"'>"+village+"</option>"; 

                 $("#blk_village").append(option); 
               }
             }

           }
        });
      });

    });
