<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('form_blockage');
});

// Route::get('/report', function () {
//     return view('report');
// });
Route::get('/report',"FormBlockageController@getBlockageTumbon");

Route::get('/index', function () {
    //return view('welcome');
    return view('index');
});

// Route::get('/up_photo', function () {
//     //return view('welcome');
//     return view('up_photo');
// });
    
// Route::get('form/create', "FormController@create")->name("form.create");
// Route::post('form/store', "FormController@store")->name("form.store");

Route::get('/up_photo',"QuestionController4@questionnaire4")->name('up_photo');

Route::get('form/questionnaire', "QuestionController@questionnaire");
Route::get('form/questionnaire2', "QuestionController2@questionnaire2")->name('form.Qnaire2');
Route::get('form/questionnaire3', "QuestionController3@questionnaire3")->name('form.Qnaire3');
Route::get('form/questionnaire4', "QuestionController4@questionnaire4")->name('form.Qnaire4');
Route::get('form/map', "MapController@map");

Route::post('form/questionnaire/store', 'QuestionController@store')->name('form.Qnaire.store');
Route::get('form/questionnaire/addBlockage', 'QuestionController@addBlockage')->name('form.Qnaire.addBlockage');
Route::get('form/questionnaire/addXsection', 'QuestionController@addXsection')->name('form.Qnaire.addXsection');
Route::get('form/questionnaire/addCulvert', 'QuestionController@addCulvert')->name('form.Qnaire.addCulvert');
Route::get('form/questionnaire/get', 'QuestionController@getData')->name('form.Qnaire.getData');
Route::get('form/questionnaire/getBlockage', 'QuestionController@getBlockageData')->name('form.Qnaire.getBlockageData');
Route::get('form/questionnaire2/addProblem', 'QuestionController2@addProblem')->name('form.Qnaire2.addProblem');

Route::get('form/questionnaire3/addSolution', 'QuestionController3@addSolution')->name('form.Qnaire3.addSolution');

Route::get('form/questionnaire/get', 'QuestionController@getData')->name('form.Qnaire.getData');
Route::get('form/questionnaire/getBlockage', 'QuestionController@getBlockageData')->name('form.Qnaire.getBlockageData');
Route::post('form/questionnaire4', "QuestionController4@uploadImage")->name('form.Qnaire4.uploadImage');

//Route::get('photo', 'PhotoController@index')->name('photo');
//Route::post('photo', 'PhotoController@uploadImage');
Route::get('/indexdistrict', 'PagesController@indexdistrict'); 
Route::get('', 'PagesController@formblockage'); 
Route::get('/getTumbol/{id}', 'PagesController@getTumbol');
Route::get('/getVillage/{id}', 'PagesController@getVillage');

//Controller Form_blockage
Route::get('/form_blockage', "FormBlockageController@formblockage");
Route::get('form/storeform', 'FormBlockageController@storeform')->name('form.storeform');
Route::get('form/getBlockage', 'FormBlockageController@getBlockageData')->name('form.getBlockageData');
Route::get('form/getBlockageMap', 'FormBlockageController@getBlockageMap')->name('form.getBlockageMap');
//Route::get('getBlockageID/{id}', 'FormBlockageController@getBlockageID')->name('getBlockageID');
Route::get('getBlockageID/{id}', ['as' => 'getBlockageID', 'uses' => 'FormBlockageController@getBlockageID']);

Route::get('/form/getBlockageTumbon/{id}', 'FormBlockageController@getBlockageTumbon');
